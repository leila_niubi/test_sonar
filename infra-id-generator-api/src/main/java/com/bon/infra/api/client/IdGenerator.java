package com.bon.infra.api.client;

public interface IdGenerator {
    long nextId();
}
