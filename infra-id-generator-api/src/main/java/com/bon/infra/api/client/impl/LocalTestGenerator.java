package com.bon.infra.api.client.impl;

import cn.hutool.Hutool;
import cn.hutool.core.lang.Snowflake;
import cn.hutool.core.util.IdUtil;
import com.bon.infra.api.client.IdGenerator;

public class LocalTestGenerator implements IdGenerator {
    @Override
    public long nextId() {
        Snowflake snowflake = IdUtil.getSnowflake();
        return snowflake.nextId();
    }
}
