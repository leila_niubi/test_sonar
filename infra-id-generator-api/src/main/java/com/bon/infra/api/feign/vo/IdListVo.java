package com.bon.infra.api.feign.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
public class IdListVo {
    @ApiModelProperty(value = "生成的id列表")
    private List<Long> ids;
}
