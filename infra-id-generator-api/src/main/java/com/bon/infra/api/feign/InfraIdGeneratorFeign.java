package com.bon.infra.api.feign;

import com.bon.framework.boot.core.common.ServerResponse;
import com.bon.infra.api.feign.vo.IdListVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author mick
 */
@FeignClient(name = "infra-id-generator-svr", url = "${infra-id-generator-svr.service.url:http://localhost:8081}", path = "/internal/infra-id-generator/v1/id")
@Api(value = "id生成器", tags = "id生成器")
public interface InfraIdGeneratorFeign {
    @GetMapping("/next-id")
    @ApiOperation(value = "获取雪花ID", notes = "获取雪花ID")
    ServerResponse<Long> nextId();

    @GetMapping("/next-id/batch")
    @ApiOperation(value = "批量获取雪花ID", notes = "批量获取雪花ID")
    ServerResponse<IdListVo> nextIdBatch(@RequestParam(value = "batchSize", required = false, defaultValue = "1000") Integer batchSize);
}
