package com.bon.infra.api.configurer;

import com.bon.infra.api.client.IdGenerator;
import com.bon.infra.api.client.impl.BufferedIdGenerator;
import com.bon.infra.api.client.impl.LocalTestGenerator;
import com.bon.infra.api.client.impl.SimpleIdGenerator;
import com.bon.infra.api.feign.InfraIdGeneratorFeign;
import com.bon.infra.api.properties.IdGeneratorProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author mick
 */
@Configuration
@EnableFeignClients("com.bon.infra.api")
@ConditionalOnProperty(value = "com.bon.infra.api.enable", matchIfMissing = true, havingValue = "true")
@EnableConfigurationProperties(IdGeneratorProperties.class)
public class InfraIdGeneratorApiAutoConfiguration {

    @Autowired
    private InfraIdGeneratorFeign idGeneratorFeign;

    @Bean
    public IdGenerator idGenerator(IdGeneratorProperties properties) {
        switch (properties.getStrategy()) {
            case BUFFERED:
                return new BufferedIdGenerator(properties, idGeneratorFeign);
            case LOCAL:
                return new LocalTestGenerator();
            case DEFAULT:
            default:
                return new SimpleIdGenerator(idGeneratorFeign);

        }
    }

}
