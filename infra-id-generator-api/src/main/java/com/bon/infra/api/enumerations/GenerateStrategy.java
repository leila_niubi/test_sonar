package com.bon.infra.api.enumerations;

/**
 * The enum Generate strategy.
 */
public enum GenerateStrategy {
    /**
     * 默认每次请求id生成器服务策略
     */
    DEFAULT,
    /**
     * 本地缓存id生成策略，适用于高性能场景
     */
    BUFFERED,
    /**
     * 本地生成策策略，仅本地单元测试时使用
     */
    LOCAL
}
