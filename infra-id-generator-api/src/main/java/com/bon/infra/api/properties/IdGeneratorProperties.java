package com.bon.infra.api.properties;


import com.bon.infra.api.enumerations.GenerateStrategy;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.concurrent.TimeUnit;

@ConfigurationProperties(prefix = "bon.id.generator")
@Data
public class IdGeneratorProperties {
    private GenerateStrategy strategy = GenerateStrategy.DEFAULT;

    private BufferedProperties buffered;

    @Data
    public static class BufferedProperties {
        private int maxBufferSize = 1000;
        private long refreshInterval = 10;
        private TimeUnit refreshTimeUit = TimeUnit.SECONDS;
        private int batchSize = 1000;
    }
}
