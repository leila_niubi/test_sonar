package com.bon.infra.api.client.impl;

import com.bon.infra.api.client.IdGenerator;
import com.bon.infra.api.feign.InfraIdGeneratorFeign;

public class SimpleIdGenerator implements IdGenerator {
    public SimpleIdGenerator(InfraIdGeneratorFeign idGeneratorFeign) {
        this.idGeneratorFeign = idGeneratorFeign;
    }

    private InfraIdGeneratorFeign idGeneratorFeign;

    @Override
    public long nextId() {
        return idGeneratorFeign.nextId().getData();
    }
}
