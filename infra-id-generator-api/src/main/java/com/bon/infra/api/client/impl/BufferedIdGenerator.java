package com.bon.infra.api.client.impl;

import com.bon.framework.boot.core.common.ResponseCode;
import com.bon.framework.boot.core.common.ServerResponse;
import com.bon.framework.boot.core.util.AssertUtil;
import com.bon.infra.api.client.IdGenerator;
import com.bon.infra.api.feign.InfraIdGeneratorFeign;
import com.bon.infra.api.feign.vo.IdListVo;

import com.bon.infra.api.properties.IdGeneratorProperties;
import com.google.common.util.concurrent.ThreadFactoryBuilder;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

import java.util.Queue;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

import static java.util.Objects.isNull;

public class BufferedIdGenerator implements IdGenerator, InitializingBean, DisposableBean {

    private IdGeneratorProperties properties;
    private ScheduledExecutorService refreshIdExecutor;

    private Queue<Long> idCache;
    private InfraIdGeneratorFeign idGeneratorFeign;

    public BufferedIdGenerator(IdGeneratorProperties properties, InfraIdGeneratorFeign idGeneratorFeign) {
        this.properties = properties;
        this.idGeneratorFeign = idGeneratorFeign;
        this.refreshIdExecutor = Executors.newScheduledThreadPool(1, new ThreadFactoryBuilder()
                .setNameFormat("id-generator-%d").build());
        AssertUtil.notNull(properties.getBuffered(), ResponseCode.INTERNAL_SERVER_ERROR, "buffer配置为空");
        AssertUtil.notNull(properties.getBuffered().getMaxBufferSize(), ResponseCode.INTERNAL_SERVER_ERROR, "buffer配置最大size为空");
        AssertUtil.notNull(properties.getBuffered().getRefreshInterval(), ResponseCode.INTERNAL_SERVER_ERROR, "buffer配置刷新时间为空");
        AssertUtil.notNull(properties.getBuffered().getRefreshTimeUit(), ResponseCode.INTERNAL_SERVER_ERROR, "buffer配置刷新时间为空");
        this.idCache = new ArrayBlockingQueue<>(properties.getBuffered().getMaxBufferSize());
    }

    @Override
    public long nextId() {
        final Long id = idCache.poll();
        if (isNull(id) || id == 0L) {
            // 缓存都用完了，就重新生成一个兜底
            refreshIdExecutor.submit(() -> refresh(properties.getBuffered()));
            return generate();
        }
        return id;
    }

    @Override
    public void destroy() throws Exception {
        refreshIdExecutor.shutdown();
    }

    private Long generate() {
        ServerResponse<Long> response = idGeneratorFeign.nextId();
        return response.getData();
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        IdGeneratorProperties.BufferedProperties buffered = properties.getBuffered();
        refreshIdExecutor.scheduleWithFixedDelay(
                () -> {
                    refresh(buffered);
                },
                buffered.getRefreshInterval(), buffered.getRefreshInterval(), buffered.getRefreshTimeUit()
        );
    }

    private void refresh(IdGeneratorProperties.BufferedProperties buffered) {
        final int targetSize = buffered.getMaxBufferSize() - idCache.size();
        int need = targetSize;
        while (need > 0) {
            int requestSize = Math.min(need, buffered.getBatchSize());
            IdListVo idListVo = idGeneratorFeign.nextIdBatch(requestSize).getData();
            idCache.addAll(idListVo.getIds());
            need = targetSize - CollectionUtils.size(idCache.size());
        }
    }
}
