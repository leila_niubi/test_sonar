package com.bon.infra.utils;

import com.bon.framework.boot.core.log.LogNames;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * https://github.com/alibaba/p3c/
 * 【强制】应用中的扩展日志（如打点、临时监控、访问日志等）命名方式：appName_logType_logName.log。
 * logType:日志类型，推荐分类有stats/desc/monitor/visit等；
 * logName:日志描述。这种命名的好处：通过文件名就可知道日志文件属于什么应用，什么类型，什么目的，也有利于归类查找。
 * 正例：mppserver应用中单独监控时区转换异常，如： mppserver_monitor_timeZoneConvert.log
 * 说明：推荐对日志进行分类，如将错误日志和业务日志分开存放，便于开发人员查看，也便于通过日志对系统进行及时监控。
 * <p>
 * 上面是阿里的规范，我们可以参考实现（appName在远程日志中心时可以启作用）。
 */
public class LogUtils {
    public static final Logger DATA_LOG = LoggerFactory.getLogger(LogNames.DATA);
    public static final Logger SERVER_LOG = LoggerFactory.getLogger(LogNames.SERVER);
    public static final Logger JOB_LOG = LoggerFactory.getLogger(LogNames.JOB);
    public static final Logger ACCESS_LOG = LoggerFactory.getLogger(LogNames.ACCESS);
    public static final Logger STATS_LOG = LoggerFactory.getLogger(LogNames.STATS);
    public static final Logger INVOKE_LOG = LoggerFactory.getLogger(LogNames.INVOKE);
}
