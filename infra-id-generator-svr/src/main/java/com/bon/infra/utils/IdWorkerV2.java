package com.bon.infra.utils;

import com.bon.framework.boot.core.util.IpUtil;
import org.apache.commons.lang3.RandomUtils;
import org.apache.commons.lang3.StringUtils;

import java.lang.management.ManagementFactory;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.sql.Timestamp;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicLong;

/**
 * tweeter的snowflake.
 * 相对IdWorker 优化datacenterId，workerId的生成，多台服务不会产生重复.
 * TODO workerId从集中化机器中获取，避免重复.
 * 1. 启动时获取 id.generator.identifier 配置，若为空则取机器hostname
 * 2. 根据 identifier 去db中获取 datacenterId，workerId (datacenterId，workerId有唯一索引)
 * 3. 若没有记录，则随机生成 datacenterId，workerId，插入； 插入主键冲突时，重复生成并插入
 * 4. 有记录时，更新最近使用时间，使用db中的 datacenterId，workerId
 * <p>
 * 结构:一共64位, 12位序列最大是4096, 单机最大tps就是400w/s
 * 0(标志位) + 41位时间戳 + 5位数据中心id + 5位workerId + 12位序列
 */
public class IdWorkerV2 {

    /* 时间起始标记点，作为基准，一般取系统的最近时间（一旦确定不能变动） */
    private final long twepoch = 1403854494756L;
    private final long workerIdBits = 5L;/* 机器标识位数 */
    private final long datacenterIdBits = 5L;
    private final long maxWorkerId = -1L ^ (-1L << workerIdBits);
    private final long maxDatacenterId = -1L ^ (-1L << datacenterIdBits);
    private final long sequenceBits = 12L;/* 毫秒内自增位 */
    private final long workerIdShift = sequenceBits;
    private final long datacenterIdShift = sequenceBits + workerIdBits;
    /* 时间戳左移动位 */
    private final long timestampLeftShift = sequenceBits + workerIdBits + datacenterIdBits;
    private final long sequenceMask = -1L ^ (-1L << sequenceBits);

    private long workerId;

    /* 数据标识id部分 */
    private long datacenterId;
    private long sequence = 0L;/* 0，并发控制 */
    private long lastTimestamp = -1L;/* 上次生产id时间戳 */

    public IdWorkerV2() {
    }

    public long getMaxDatacenterId() {
        return maxDatacenterId;
    }

    public long getMaxWorkerId() {
        return maxWorkerId;
    }

    public void setDatacenterId(long datacenterId) {
        this.datacenterId = datacenterId;
    }

    public void setWorkerId(long workerId) {
        this.workerId = workerId;
    }

    public long getWorkerId() {
        return workerId;
    }

    public long getDatacenterId() {
        return datacenterId;
    }

    //    /**
//     * @param workerId
//     *            工作机器ID
//     * @param datacenterId
//     *            序列号
//     */
//    public IdWorkerV2(long workerId, long datacenterId) {
//        if (workerId > maxWorkerId || workerId < 0) {
//            throw new IllegalArgumentException(
//                    String.format("worker Id can't be greater than %d or less than 0", maxWorkerId));
//        }
//        if (datacenterId > maxDatacenterId || datacenterId < 0) {
//            throw new IllegalArgumentException(
//                    String.format("datacenter Id can't be greater than %d or less than 0", maxDatacenterId));
//        }
//        this.workerId = workerId;
//        this.datacenterId = datacenterId;
//        log.info("IdWorkerV2 init. dataId:{}, workId:{}", datacenterId, workerId);
//    }

    /**
     * <p>
     * 获取 maxWorkerId
     * </p>
     */
    public static long getMaxWorkerId(long datacenterId, long maxWorkerId) {
        StringBuilder mpid = new StringBuilder();
        mpid.append(datacenterId);
        String name = ManagementFactory.getRuntimeMXBean().getName();
        if (StringUtils.isNotEmpty(name)) {
            /*
             * GET jvmPid
             */
            mpid.append(name.split("@")[0]);
        }
        /*
         * MAC + PID 的 hashcode 获取16个低位
         */
        return (mpid.toString().hashCode() & 0xffff) % (maxWorkerId + 1);
    }

    /**
     * <p>
     * 数据标识id部分
     * </p>
     */
    public static long getDatacenterId(long maxDatacenterId) {
        long id = 0L;
        try {
            InetAddress ip = IpUtil.getFirstNonLoopbackAddress(true, false);
            LogUtils.SERVER_LOG.info("获取到本机的ip:{}", ip);
            NetworkInterface network = NetworkInterface.getByInetAddress(ip);
            if (network == null) {
                id = RandomUtils.nextLong(0, maxDatacenterId);
            } else {
                byte[] mac = network.getHardwareAddress();
                id = ((0x000000FF & (long) mac[mac.length - 1])
                        | (0x0000FF00 & (((long) mac[mac.length - 2]) << 8))) >> 6;
                id = id % (maxDatacenterId + 1);
            }
        } catch (Exception e) {
            LogUtils.SERVER_LOG.error("生产DatacenterId失败:", e);
            id = RandomUtils.nextLong(0, maxDatacenterId);
        }
        LogUtils.SERVER_LOG.info("getDatacenterId:{}", id);
        return id;
    }

    /**
     * 获取下一个ID
     *
     * @return
     */
    public synchronized long nextId() {
        long timestamp = timeGen();
        if (timestamp < lastTimestamp) {// 闰秒
            long offset = lastTimestamp - timestamp;
            LogUtils.DATA_LOG.info("nextId.clock timeStamp {}, lastTimestamp {}, offset {}", timestamp, lastTimestamp, offset);
            // todo 告警通知

            if (offset <= 5) {
                try {
                    LogUtils.DATA_LOG.info("nextId.clock begin wait timeStamp {}, lastTimestamp {}", timestamp, lastTimestamp);
                    wait(offset << 1);
                    timestamp = timeGen();
                    LogUtils.DATA_LOG.info("nextId.clock after wait timeStamp {}, lastTimestamp {}", timestamp, lastTimestamp);
                    if (timestamp < lastTimestamp) {
                        throw new RuntimeException(String
                                .format("Clock moved backwards.  Refusing to generate id for %d milliseconds", offset));
                    }
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }

                LogUtils.DATA_LOG.info("nextId.clock timestamp gen {}, lastTimestamp {}", timestamp, lastTimestamp);
            } else {
                LogUtils.DATA_LOG.info("nextId.clock timeStamp {}, lastTimestamp {}, offset {}, throw exception", timestamp, lastTimestamp, offset);
                throw new RuntimeException(
                        String.format("Clock moved backwards.  Refusing to generate id for %d milliseconds", offset));
            }

        }

        if (lastTimestamp == timestamp) {
            // 相同毫秒内，序列号自增
            sequence = (sequence + 1) & sequenceMask;
            if (sequence == 0) {
                // 同一毫秒的序列数已经达到最大
                timestamp = tilNextMillis(lastTimestamp);
            }
        } else {
            // 不同毫秒内，序列号置为 0 - 9 随机数
            sequence = ThreadLocalRandom.current().nextLong(0, 9);
        }

        lastTimestamp = timestamp;

        return ((timestamp - twepoch) << timestampLeftShift) // 时间戳部分
                | (datacenterId << datacenterIdShift) // 数据中心部分
                | (workerId << workerIdShift) // 机器标识部分
                | sequence; // 序列号部分
    }

    protected long tilNextMillis(long lastTimestamp) {
        long timestamp = timeGen();
        while (timestamp <= lastTimestamp) {
            timestamp = timeGen();
        }
        return timestamp;
    }

    protected long timeGen() {
        return SystemClock.now();
    }
}

class SystemClock {

    private final long period;
    private final AtomicLong now;

    private SystemClock(long period) {
        this.period = period;
        this.now = new AtomicLong(System.currentTimeMillis());
        scheduleClockUpdating();
    }

    private static SystemClock instance() {
        return InstanceHolder.INSTANCE;
    }

    public static long now() {
        return instance().currentTimeMillis();
    }

    public static String nowDate() {
        return new Timestamp(instance().currentTimeMillis()).toString();
    }

    private void scheduleClockUpdating() {
        ScheduledExecutorService scheduler = Executors.newSingleThreadScheduledExecutor(new ThreadFactory() {
            @Override
            public Thread newThread(Runnable runnable) {
                Thread thread = new Thread(runnable, "System Clock");
                thread.setDaemon(true);
                return thread;
            }
        });
        scheduler.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {

                now.set(System.currentTimeMillis());
            }
        }, period, period, TimeUnit.MILLISECONDS);
    }

    private long currentTimeMillis() {
        return now.get();
    }

    private static class InstanceHolder {

        public static final SystemClock INSTANCE = new SystemClock(1);
    }

}
