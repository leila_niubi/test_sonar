package com.bon.infra;

import bon.framework.boot.autoconfigure.monitor.AsyncAyWeixinNotify;
import com.bon.framework.boot.core.env.ConfigUtils;
import com.bon.infra.utils.LogUtils;
import com.coinsea.monitor.notify.NotifyClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @author mick
 */
@SpringBootApplication(scanBasePackages = {"com.bon.infra"})
@EnableFeignClients
//@EnableLoadTimeWeaving
public class InfraIdGeneratorApplication implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(InfraIdGeneratorApplication.class, args);
    }

    @Autowired
    private ConfigUtils configUtils;

    @Override
    public void run(String... args) throws Exception {
        initNotifyServer();
        LogUtils.SERVER_LOG.info("{} start success", InfraIdGeneratorApplication.class.getSimpleName());
        AsyncAyWeixinNotify.send("{} start success!", InfraIdGeneratorApplication.class.getSimpleName());
    }

    private void initNotifyServer() {
        try {
            NotifyClient.init(configUtils.getString("notifyserver.url"), "app.TradingBotServer",
                    configUtils.getString("notifyserver.appKey"), configUtils.getString("notifyserver.appSecret"));

            LogUtils.DATA_LOG.info("initNotifyServer finish.url:{}", configUtils.getString("notifyserver.url"));

        } catch (Exception er) {
            LogUtils.DATA_LOG.warn("initNotifyServer has exception.er:", er);
        }
    }
}
