package com.bon.infra.constants;

/**
 * @author mick
 */
public interface DbConstants {
    String BON_API_WRITE = "serverWrite";
    String SERVER_READ = "serverRead";
}
