package com.bon.infra.constants;

/**
 * @author mick
 */
public interface MyConstants {

    enum MyEnum {
        /**
         * value1,value2
         */
        VALUE1, VALUE2
    }

    int MY_INT = 1;

}
