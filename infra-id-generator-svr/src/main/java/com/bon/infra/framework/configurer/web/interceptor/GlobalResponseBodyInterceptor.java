package com.bon.infra.framework.configurer.web.interceptor;

import bon.framework.boot.autoconfigure.apollo.util.IGlobalConfig;
import bon.framework.boot.autoconfigure.web.constants.HttpConfigConst;
import cn.hutool.core.util.StrUtil;
import com.bon.framework.boot.core.common.ResponseCode;
import com.bon.framework.boot.core.common.ServerResponse;
import com.bon.framework.boot.core.exception.AppException;
import com.google.common.collect.Lists;
import com.google.gson.Gson;
import org.springframework.core.MethodParameter;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.util.AntPathMatcher;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

import static com.bon.infra.utils.LogUtils.DATA_LOG;

/**
 * @author flynn
 * 对结果进行标准化处理
 */
@ControllerAdvice
public class GlobalResponseBodyInterceptor implements ResponseBodyAdvice<Object> {
    /**
     * 路径匹配器
     */
    private static final AntPathMatcher PATH_MATCHER = new AntPathMatcher();
    /**
     * 默认需要检查想对象
     */
    private static final List<String> DEFAULT_RSP_PACKING_ULR_PATTERN = Lists.newArrayList("/api/**", "/internal/**");
    /**
     * 动态适配的key
     */
    private static final String RSP_PACKING_ULR_PATTERN_KEY = "RSP_PACKING_ULR_PATTERN";
    /**
     * 分隔符
     */
    private static final String SPLIT_SEPARATOR = ",";

    @Resource
    private IGlobalConfig globalConfig;

    @ExceptionHandler(value = Exception.class)
    @ResponseBody
    public ServerResponse<String> jsonErrorHandler(HttpServletRequest req, Exception e) {
        try {
            DATA_LOG.error(StrUtil.format("GlobalExceptionHandler exception happen uri : {}, request body {}", req.getRequestURI()), e);

            if (e instanceof IllegalStateException || e instanceof MissingServletRequestParameterException || e instanceof HttpMessageNotReadableException || e instanceof IllegalArgumentException || e instanceof MethodArgumentNotValidException || e instanceof MethodArgumentTypeMismatchException) {
                DATA_LOG.warn("uri {}, illegal arguments ", req.getRequestURI(), e);
                return ServerResponse.create(ResponseCode.ILLEGAL_ARGUMENT.getCode(), "miss arguments");
            }
            if (e instanceof AppException) {
                AppException appException = (AppException) e;
                return ServerResponse.createByErrorCodeMsg(appException.getCode(), appException.getCodeMessage());
            }
            return ServerResponse.createByErrorCodeMsg(ResponseCode.INTERNAL_SERVER_ERROR.getCode(), "INTERNAL_SERVER_ERROR");
        } catch (Exception ex) {
            DATA_LOG.warn("GlobalExceptionHandler.jsonErrorHandler exception :", ex);
            return ServerResponse.createByErrorCodeMsg(ResponseCode.INTERNAL_SERVER_ERROR.getCode(), "INTERNAL_SERVER_ERROR");
        }
    }

    @Override
    public boolean supports(MethodParameter returnType, Class<? extends HttpMessageConverter<?>> converterType) {
        return true;
    }

    /**
     * 对返回的参数做统一的拦截处理和封装
     * 并且进行国际化处理
     */
    @Override
    public Object beforeBodyWrite(Object body, MethodParameter returnType, MediaType selectedContentType, Class<? extends HttpMessageConverter<?>> selectedConverterType, ServerHttpRequest request, ServerHttpResponse response) {
        if (!isNeedPackingRsp(request)) {
            return body;
        }
        if (body == null) {
            return ServerResponse.createBySuccess(null);
        }
        //这个地方需要注意string和对象走的不是一样的messageConverter
        if (body.getClass().isPrimitive() || body instanceof String) {
            return new Gson().toJson(ServerResponse.createBySuccess(body));
        }
        if (body instanceof ServerResponse) {
            return body;
        }
        if (request.getURI().getPath().startsWith(HttpConfigConst.SERVICE_API_PATH_PREFIX)) {
            return ServerResponse.createBySuccess(body);
        }
        return body;
    }

    /**
     * 不需要进行返回内容的包装
     *
     * @param request 请求的request
     * @return true 不需要 false 需要
     */
    private boolean isNeedPackingRsp(ServerHttpRequest request) {
        List<String> patternList = globalConfig.getListWithCache(RSP_PACKING_ULR_PATTERN_KEY, SPLIT_SEPARATOR, DEFAULT_RSP_PACKING_ULR_PATTERN);
        if (patternList == null || patternList.size() == 0) {
            return false;
        }

        return patternList.stream().anyMatch(pattern -> PATH_MATCHER.match(pattern, request.getURI().getPath()));
    }
}