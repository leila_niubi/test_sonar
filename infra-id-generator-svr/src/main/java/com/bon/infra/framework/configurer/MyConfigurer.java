package com.bon.infra.framework.configurer;

import bon.framework.boot.autoconfigure.sns.annotation.client.EnableSnsClient;
import bon.framework.boot.autoconfigure.sqs.annotation.client.EnableSqsClient;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Configuration;

/**
 * @author admin
 */
@Configuration
@EnableCaching
@MapperScan("com.bon.infra.dal.dao.mapper")
@EnableSnsClient(basePackages = {"com.bon.infra.dal.message.sns"})
@EnableSqsClient(basePackages = {"com.bon.infra.dal.message.sqs"})
public class MyConfigurer {
}
