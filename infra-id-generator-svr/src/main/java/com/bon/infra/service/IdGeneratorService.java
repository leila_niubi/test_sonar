package com.bon.infra.service;

import com.bon.infra.api.feign.vo.IdListVo;

/**
 * @author mick
 */
public interface IdGeneratorService {
    long nextId();

    IdListVo nextIdBatch(Integer batchSize);
}
