package com.bon.infra.service.impl;

import bon.framework.boot.autoconfigure.apollo.util.IGlobalConfig;
import bon.framework.boot.autoconfigure.metrics.BizMetrics;
import com.bon.framework.boot.core.env.ConfigUtils;
import com.bon.framework.boot.core.util.IpUtil;
import com.bon.infra.api.feign.vo.IdListVo;
import com.bon.infra.dal.dao.entity.TbIdRegisterInfo;
import com.bon.infra.dal.dao.service.TbIdRegisterInfoService;
import com.bon.infra.utils.IdWorkerV2;
import com.bon.infra.utils.LogUtils;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import io.micrometer.core.annotation.Timed;
import org.apache.commons.lang3.RandomUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Service;
import com.bon.infra.service.IdGeneratorService;

import java.net.InetAddress;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * @author mick
 */
@Service
public class IdGeneratorServiceImpl implements IdGeneratorService, InitializingBean {

    @Autowired
    private IGlobalConfig globalConfig;

    private IdWorkerV2 idWorker;

    @Autowired
    private ConfigUtils configUtils;

    @Autowired
    private TbIdRegisterInfoService tbIdRegisterInfoService;
    @Autowired
    private BizMetrics bizMetrics;

    @Override
    @Timed(value = "generate-id-cost", percentiles = {0.99}, extraTags = {BizMetrics.PUSH_TAG_KEY, BizMetrics.PUSH_TAG})
    public long nextId() {
        bizMetrics.summaryRecordOne("generate-id");
        return idWorker.nextId();
    }

    @Override
    @Timed(value = "generate-id-cost-batch", percentiles = {0.99}, extraTags = {BizMetrics.PUSH_TAG_KEY, BizMetrics.PUSH_TAG})
    public IdListVo nextIdBatch(Integer batchSize) {
        IdListVo idListVo = new IdListVo();
        List<Long> ids = Lists.newArrayList();
        long max = Math.min(globalConfig.getLong("id.generator.max.fetch.size", 1000L), batchSize);
        for (int i = 0; i < max; i++) {
            ids.add(nextId());
        }
        idListVo.setIds(ids);
        return idListVo;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        this.idWorker = new IdWorkerV2();
        long datacenterId = 0;
        long wokerId = 0;
        // workerId从集中化机器中获取，避免重复.
        // 1. 启动时获取 id.generator.identifier 配置，若为空则取机器hostname
        // 2. 根据 identifier 去db中获取 datacenterId，workerId (datacenterId，workerId有唯一索引tb_id_register_info)
        // 3. 若没有记录，则随机生成 datacenterId，workerId，插入； 插入主键冲突时，重复生成并插入
        // 4. 有记录时，更新最近使用时间，使用db中的 datacenterId，workerId
        if (configUtils.getBoolean("workerIdAllocateFromDb", true)) {
            String workerIdentifier = getWorkerIdentifier();
            TbIdRegisterInfo registerInfo = tbIdRegisterInfoService.getByIdentifier(workerIdentifier);
            if (registerInfo == null) {
                registerInfo = generateRegisterInfo(workerIdentifier, idWorker, 0);

                if (registerInfo == null) {
                    throw new RuntimeException("db中获取并生成datacenterId，workerId失败，服务不能启动.");
                }
            } else {
                tbIdRegisterInfoService.updateLastUseTime(workerIdentifier, Strings.nullToEmpty(IpUtil.getIp()));
            }

            datacenterId = registerInfo.getDataCenterId().longValue();
            wokerId = registerInfo.getWorkerId().longValue();

            LogUtils.DATA_LOG.info("initIdWorker workerIdentifier:{}", workerIdentifier);
        } else {
            datacenterId = IdWorkerV2.getDatacenterId(idWorker.getMaxDatacenterId());
            wokerId = IdWorkerV2.getMaxWorkerId(datacenterId, idWorker.getMaxWorkerId());
        }

        if (wokerId > idWorker.getMaxWorkerId() || wokerId < 0) {
            throw new IllegalArgumentException(String.format("worker Id can't be greater than %d or less than 0", idWorker.getMaxWorkerId()));
        }
        if (datacenterId > idWorker.getMaxDatacenterId() || datacenterId < 0) {
            throw new IllegalArgumentException(String.format("datacenter Id can't be greater than %d or less than 0", idWorker.getMaxDatacenterId()));
        }

        idWorker.setDatacenterId(datacenterId);
        idWorker.setWorkerId(wokerId);


        LogUtils.SERVER_LOG.info("initIdWorker finish. dataId:{}, workId:{}, maxWorkId {}, maxDataCenterId {}", datacenterId, wokerId, idWorker.getMaxWorkerId(), idWorker.getMaxDatacenterId());
    }

    private TbIdRegisterInfo generateRegisterInfo(String workerIdentifier, IdWorkerV2 idWorker, int retryTimes) {
        long datacenterId = RandomUtils.nextLong(0, idWorker.getMaxDatacenterId());
        long wokerId = RandomUtils.nextLong(0, idWorker.getMaxWorkerId());

        TbIdRegisterInfo registerInfo = new TbIdRegisterInfo();
        registerInfo.setIdentifier(workerIdentifier);
        registerInfo.setDataCenterId((byte) datacenterId);
        registerInfo.setWorkerId((byte) wokerId);
        registerInfo.setLastUseIp(Strings.nullToEmpty(IpUtil.getIp()));
        registerInfo.setRemark("");
        registerInfo.setLastUseTime(new Date());
        registerInfo.setCreateTime(new Date());
        registerInfo.setUpdateTime(new Date());

        try {
            tbIdRegisterInfoService.save(registerInfo);
            return registerInfo;
        } catch (DuplicateKeyException er) {
            retryTimes = retryTimes++;

            if (retryTimes > 10) {
                LogUtils.SERVER_LOG.warn("generateRegisterInfo has DuplicateKeyException no need retry. registerInfo:{}, retryTimes:{}, er:{}",
                        registerInfo, retryTimes, er);
                return null;
            } else {
                LogUtils.SERVER_LOG.warn("generateRegisterInfo has DuplicateKeyException need retry. registerInfo:{}, retryTimes:{}, er:{}",
                        registerInfo, retryTimes, er);
                return generateRegisterInfo(workerIdentifier, idWorker, retryTimes);
            }
        }
    }

    private String getWorkerIdentifier() {
        String workerIdentifier = configUtils.getString("id.generator.identifier");
        if (StringUtils.isEmpty(workerIdentifier)) {
            try {
                workerIdentifier = InetAddress.getLocalHost().getHostName();

                if (StringUtils.isEmpty(workerIdentifier)) {
                    workerIdentifier = IpUtil.getIp();
                }
            } catch (Exception er) {
                LogUtils.SERVER_LOG.warn("getWorkerIdentifier has exception.", er);
            }
        }

        if (StringUtils.isEmpty(workerIdentifier)) {
            workerIdentifier = UUID.randomUUID().toString();
            LogUtils.SERVER_LOG.warn("getWorkerIdentifier is empty so return uuid.{}", workerIdentifier);
        }
        return workerIdentifier;
    }


}
