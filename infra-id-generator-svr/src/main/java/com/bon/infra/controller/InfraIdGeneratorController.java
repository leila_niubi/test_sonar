package com.bon.infra.controller;

import com.bon.framework.boot.core.common.ServerResponse;
import com.bon.infra.api.feign.InfraIdGeneratorFeign;
import com.bon.infra.api.feign.vo.IdListVo;
import com.bon.infra.service.IdGeneratorService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author mick
 */
@RestController
@RequestMapping("/internal/infra-id-generator/v1/id")
@Api(tags = "id生成器")
public class InfraIdGeneratorController implements InfraIdGeneratorFeign {

    @Autowired
    private IdGeneratorService idGeneratorService;

    @Override
    public ServerResponse<Long> nextId() {
        return ServerResponse.createBySuccess(idGeneratorService.nextId());
    }

    @Override
    public ServerResponse<IdListVo> nextIdBatch(Integer batchSize) {
        return ServerResponse.createBySuccess(idGeneratorService.nextIdBatch(batchSize));
    }
}
