package com.bon.infra.controller;

import com.bon.framework.boot.core.common.ServerResponse;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author mick
 */
@RestController
public class HealthController {
    @GetMapping("/health")
    ServerResponse<?> checkHealth() {
        return ServerResponse.createBySuccess();
    }
}
