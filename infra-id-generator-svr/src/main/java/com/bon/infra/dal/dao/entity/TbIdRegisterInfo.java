package com.bon.infra.dal.dao.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.util.Date;
import lombok.Data;

/**
 * id生成器的注册信息(datacenterId,workerId)
 */
@Data
@TableName(value = "tb_id_register_info")
public class TbIdRegisterInfo {
    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 机器进程的标识
     */
    @TableField(value = "identifier")
    private String identifier;

    /**
     * datacenterId
     */
    @TableField(value = "data_center_id")
    private Byte dataCenterId;

    /**
     * workerId
     */
    @TableField(value = "worker_id")
    private Byte workerId;

    /**
     * 最近使用时间
     */
    @TableField(value = "last_use_time")
    private Date lastUseTime;

    /**
     * 最近使用的ip
     */
    @TableField(value = "last_use_ip")
    private String lastUseIp;

    /**
     * 备注
     */
    @TableField(value = "remark")
    private String remark;

    /**
     * 创建时间
     */
    @TableField(value = "create_time")
    private Date createTime;

    /**
     * 最后一次更新时间
     */
    @TableField(value = "update_time")
    private Date updateTime;

    public static final String COL_ID = "id";

    public static final String COL_IDENTIFIER = "identifier";

    public static final String COL_DATA_CENTER_ID = "data_center_id";

    public static final String COL_WORKER_ID = "worker_id";

    public static final String COL_LAST_USE_TIME = "last_use_time";

    public static final String COL_LAST_USE_IP = "last_use_ip";

    public static final String COL_REMARK = "remark";

    public static final String COL_CREATE_TIME = "create_time";

    public static final String COL_UPDATE_TIME = "update_time";
}