package com.bon.infra.dal.dao.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bon.infra.dal.dao.entity.TbIdRegisterInfo;
import com.bon.infra.dal.dao.mapper.TbIdRegisterInfoMapper;
import com.bon.infra.dal.dao.service.TbIdRegisterInfoService;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class TbIdRegisterInfoServiceImpl extends ServiceImpl<TbIdRegisterInfoMapper, TbIdRegisterInfo> implements TbIdRegisterInfoService {

    @Override
    public int batchInsert(List<TbIdRegisterInfo> list) {
        return baseMapper.batchInsert(list);
    }

    @Override
    public TbIdRegisterInfo getByIdentifier(String identifier) {
        LambdaQueryWrapper<TbIdRegisterInfo> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(TbIdRegisterInfo::getIdentifier, identifier);
        return baseMapper.selectOne(wrapper);
    }

    @Override
    public int updateLastUseTime(String identifier, String lastUseIp) {
        LambdaUpdateWrapper<TbIdRegisterInfo> wrapper = new LambdaUpdateWrapper<>();
        Date now = new Date();
        wrapper.eq(TbIdRegisterInfo::getIdentifier, identifier);
        wrapper.set(TbIdRegisterInfo::getLastUseIp, lastUseIp);
        wrapper.set(TbIdRegisterInfo::getLastUseTime, now);
        wrapper.set(TbIdRegisterInfo::getUpdateTime, now);
        return baseMapper.update(null, wrapper);
    }
}
