package com.bon.infra.dal.dao.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.bon.infra.dal.dao.entity.TbIdRegisterInfo;

import java.util.List;

public interface TbIdRegisterInfoService extends IService<TbIdRegisterInfo> {


    int batchInsert(List<TbIdRegisterInfo> list);

    TbIdRegisterInfo getByIdentifier(String identifier);

    int updateLastUseTime(String identifier, String lastUseIp);

}
