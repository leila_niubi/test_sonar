package com.bon.infra.dal.dao.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bon.infra.dal.dao.entity.TbIdRegisterInfo;

import java.util.List;

import org.apache.ibatis.annotations.Param;

public interface TbIdRegisterInfoMapper extends BaseMapper<TbIdRegisterInfo> {
    int batchInsert(@Param("list") List<TbIdRegisterInfo> list);
}