create table tb_coupon_type
(
    `id`            int(11)     NOT NULL COMMENT '自增id' AUTO_INCREMENT primary key,
    `type`          tinyint     NOT NULL COMMENT '券类型(跟线上保持一致)1-赠金 2-返现券 3-跟单保险金',
    `ch_desc`       varchar(32) NOT NULL COMMENT '中文描述',
    `display_order` int         NOT NULL COMMENT '展示顺序越小越靠前 赠金>跟单保险金>返现券',
    `create_time`   timestamp   NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `update_time`   timestamp   NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
    unique index tb_coupon_type_u_idx (`type`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4 COMMENT ='券类型表';


create table tb_coupon_content_template
(
    `id`            int          NOT NULL COMMENT '自增id' AUTO_INCREMENT primary key,
    `type`          tinyint      NOT NULL COMMENT '券类型(跟线上保持一致)1-赠金 2-返现券 3-跟单保险金',
    `template_name` varchar(255) NOT NULL DEFAULT '' COMMENT '模板名字',
    `template_type` tinyint(4)   NOT NULL DEFAULT '0' COMMENT '临时模板 0, 用户建立模板 1',
    `create_time`   timestamp    NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `update_time`   timestamp    NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间'
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4 COMMENT ='券内容模板表';

create table tb_coupon_batch_template
(
    `id`            int         NOT NULL COMMENT '自增id' AUTO_INCREMENT primary key,
    `type`          tinyint     NOT NULL COMMENT '券类型(跟线上保持一致)1-赠金 2-返现券 3-跟单保险金',
    `title_key`     varchar(32) NOT NULL DEFAULT '' COMMENT '标题key',
    `sub_title_key` varchar(32) NOT NULL DEFAULT '' COMMENT '副标题key',
    `create_time`   timestamp   NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `update_time`   timestamp   NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间'

) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4 COMMENT ='券批次模板表';

create table tb_coupon_rule
(
    `id`           int(11)   NOT NULL COMMENT '批次id' AUTO_INCREMENT primary key,
    `type`         tinyint   NOT NULL COMMENT '券类型(跟线上保持一致)1-赠金 2-返现券 3-跟单保险金',
    `rule_content` longtext  NOT NULL DEFAULT '' COMMENT '规则详情 JSON格式跟随type 条件如 金额限制，领取起止日期，使用起止日期，有效期天数，',
    `validate_type`  byte  NOT NULL COMMENT '有效期类型',
    `valid_begin_date` timestamp           NOT NULL COMMENT '活动有效期开始',
    `valid_end_date`   timestamp           NOT NULL COMMENT '有效期结束时间',
    `validate_offset`  int  NOT NULL COMMENT '有效期天数',
    `create_time`  timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `update_time`  timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
    unique index tb_coupon_type_u_idx (`type`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4 COMMENT ='券规则表';

create table tb_coupon_batch
(
    `id`                 int(11)             NOT NULL COMMENT '批次id' AUTO_INCREMENT primary key,
    `type`               tinyint             NOT NULL COMMENT '券类型(跟线上保持一致)1-赠金 2-返现券 3-跟单保险金',
    `batch_template_id`  int(11)             NOT NULL COMMENT '模板id，表tb_fee_rebate_template 的id',
    `rule_id`            int(11)             NOT NULL COMMENT '券规则id，表',
    `platform_id`        tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '平台类型 0-平台通用 1-标准合约 2-专业合约 3-现货 4-标准合约跟单 5-币安跟单 6-专业合约跟单 7-现货网格跟单',
    `amount`             decimal(22, 13)     NOT NULL COMMENT '面额',
    `total_amount`       decimal(22, 13)     NOT NULL COMMENT '总面额',
    `total_count`        int(11)             NOT NULL COMMENT '总数 -1 不限制',
    `limited_user_count` int(11)             NOT NULL COMMENT '无效用户总数',
    `proposer`           varchar(128)        NOT NULL COMMENT '申请人',
    `auditor`            varchar(128)        NOT NULL DEFAULT '' COMMENT '审核人',
    `remark`             varchar(255)        NOT NULL DEFAULT '' COMMENT '备注',
    `status`             tinyint(4)          NOT NULL DEFAULT '1' COMMENT '状态： 1: 审核中 2: 已通过 3: 已拒绝 4：已撤销',
    `receive_users`      longtext            NULL COMMENT '接收用户 空表示用户领取类型',
    `user_type`          tinyint(4)          NULL COMMENT '用户类型 1 账号 2 uid',
    `send_push`          tinyint(4)          NOT NULL COMMENT '是否发push 0 不发 1 发',
    `send_notification`  tinyint(4)          NOT NULL COMMENT '是否发站内信 0 不发 1 发',
    `propose_time`       timestamp           NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '申请时间',
    `audit_time`         timestamp           NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '审核时间',
    `audit_remark`       varchar(255)        NOT NULL DEFAULT '' COMMENT '审核备注',
    `create_time`        timestamp           NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `update_time`        timestamp           NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
    unique index tb_coupon_type_u_idx (`type`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4 COMMENT ='券批次表';

CREATE TABLE `tb_user_coupon`
(
    `id`               bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键' PRIMARY KEY,
    `uid`              bigint(20)          NOT NULL DEFAULT '0' COMMENT '用户uid',
    `type`             tinyint             NOT NULL COMMENT '券类型(跟线上保持一致)1-赠金 2-返现券 3-跟单保险金',
    `asset_id`         int(11)             NOT NULL COMMENT '资产id',
    `template_id`      int(11)             NOT NULL DEFAULT '0' COMMENT '关联模板tb_coupon_batch_template.id',
    `batch_id`         int(11)             NOT NULL DEFAULT '0' COMMENT '关联批次tb_coupon_batch.id',
    `rule_id`          int(11)             NOT NULL DEFAULT '0' COMMENT '关联规则tb_coupon_rule.id',
    `activity_id`      int(11)             NOT NULL DEFAULT '0' COMMENT '关联领取的活动id',
    `platform_id`      tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '平台类型 0-平台通用 1-标准合约 2-专业合约 3-现货 4-标准合约跟单 5-币安跟单 6-专业合约跟单 7-现货网格跟单',
    `amount`           decimal(22, 13)     NOT NULL DEFAULT '0.0000000000000' COMMENT '金额',
    `available_amount` decimal(22, 13)     NOT NULL DEFAULT '0.0000000000000' COMMENT '可用金额',
    `valid_begin_date` timestamp           NOT NULL COMMENT '活动有效期开始',
    `valid_end_date`   timestamp           NOT NULL COMMENT '有效期结束时间',
    `receive_date`     timestamp           NOT NULL COMMENT '接收/领取时间',
    `use_date`         timestamp           NOT NULL COMMENT '使用时间',
    `status`           tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '0-待生效 1-已生效 2-已用完 3-已过期',
    `create_time`      timestamp           NOT NULL COMMENT '创建时间',
    `update_time`      timestamp           NOT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
    KEY `idx_template_id` (`template_id`),
    KEY `idx_uid_status` (`uid`, `status`),
    KEY `idx_status` (`status`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 307279
  DEFAULT CHARSET = utf8mb4 COMMENT ='用户券表';

CREATE TABLE `tb_user_coupon_log`
(
    `id`              bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
    `uid`             bigint(20)          NOT NULL DEFAULT '0' COMMENT '用户uid',
    `user_coupon_id`  bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '关联tb_user_coupon.id',
    `asset_id`        int(11)             NOT NULL COMMENT '资产id',
    `amount`          decimal(22, 13)     NOT NULL DEFAULT '0.0000000000000' COMMENT '金额',
    `platform_id`     tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '平台类型 0-平台通用 1-标准合约 2-专业合约 3-现货 4-标准合约跟单 5-币安跟单 6-专业合约跟单 7-现货网格跟单',
    `order_no`        bigint(20)          NOT NULL COMMENT '关联的业务订单号',
    `origin_asset_id` int(11)                      DEFAULT NULL COMMENT '原始的资产id',
    `origin_amount`   decimal(22, 13)              DEFAULT NULL COMMENT '原始的金额',
    `create_time`     timestamp           NOT NULL COMMENT '创建时间',
    PRIMARY KEY (`id`),
    UNIQUE KEY `uk_order_no_record_id` (`order_no`, `user_coupon_id`),
    KEY `idx_uid` (`uid`),
    KEY `idx_fee_rebate_record_id` (`user_coupon_id`),
    KEY `idx_create_time` (`create_time`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4 COMMENT ='用户券使用日志';